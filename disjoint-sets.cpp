#include <stdio.h>
#include <stdint.h>
#include <conio.h>
#include <stdlib.h>

#define nr_nodes_test 9

struct node{

    int rank;
    int data;
    node* p;

};


void make_set(node* x)
{

    x->p = x;
    x->rank = 0;

}

void link_sets(node* x, node* y)
{

    if (x->rank > y->rank)
        y->p = x;
    else
    {

        x->p = y;
        if (x->rank == y->rank)
            y->rank++;

    }

}

node* find_set(node* x)
{

    if (x != x->p)
        x->p = find_set(x->p);
    return x->p;

}

void union_sets(node* x, node* y)
{

    link_sets(find_set(x), find_set(y));

}

void test_sets()
{

    struct node* v[nr_nodes_test];

    for (int i = 0; i < nr_nodes_test; i++)
    {

        v[i] = (node*)malloc(sizeof(node));
        v[i]->data = i;
        make_set(v[i]);

    }

    for (int i = 0; i < nr_nodes_test; i++)
    {

        printf("node: %d  data: %d  parent: %d  rank: %d", v[i], v[i]->data, v[i]->p, v[i]->rank);
        printf("\n");

    }

    printf("\n\n");

    printf("Union between the set {0} and set {1} \n");
    union_sets(v[0], v[1]);
    printf("The representative of the element with the key 0: %d\n\n", find_set(v[1])->data);

    printf("Union between the set {1,0} and set {2} \n");
    union_sets(v[0], v[2]);
    printf("The representative of the element with the key 2: %d\n\n", find_set(v[2])->data); // {1,0,2}

    printf("Union between the set {5} and set {6} \n");
    union_sets(v[5], v[6]);
    printf("The representative of the element with the key 5: %d\n\n", find_set(v[5])->data);  //{6,5}

    printf("Union between the set {6,5} and set {7} \n");
    union_sets(v[6], v[7]);
    printf("The representative of the element with the key 7: %d\n\n", find_set(v[7])->data);  //{6,5,7}

    printf("Union between the set {1,0,2} and set {6,7,5} \n");
    union_sets(v[1], v[6]);
    printf("The representative of the element with the key 1: %d\n\n", find_set(v[1])->data);  //{6,5,7,1,0,2}

}

int main()
{
    test_sets();

    return 0;
}
