﻿/*Scrie un program care pentru fiecare n din intervalul{ 100, 200, …, 10.000 } 
calculează și adaugă într-un fișier următoarele valori :
n, 100 * log(n), 10 * n, n*log(n), 0.1*n2, 0.01*n3*/
//Folosește valorile din fișier ca să generezi un grafic în funcție de n.
#include<stdio.h>
#include<fstream> //biblioteca pentru fisiere
#include<stdlib.h>



int main() {
	
	FILE *f = fopen("file.csv", "w");
	fprintf(f, "n, 100 * log(n), 10 * n, n*log(n), 0.1*n, 0.01*n* \n");
		

	for (int n = 100; n < 10000; n = n + 100) {
		fprintf(f, "%d, %d, %d, %d, %d, %d \n", n, 100 * log(n), 10 * n, n*log(n), 0.1*n, 0.01*n);
	}
	
	fclose(f);
	return 0;


}