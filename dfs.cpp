

#include "Profiler.h"
#include <stdio.h>
#include <random>
#include <conio.h>
#include<iostream>
#include <iomanip>
#include <fstream>

using namespace std;
Profiler profiler("DFS Algorithm");

struct node // structura nodului
{
    int key;
    int d; // timpul cand e descoperit
    int f; // timpul de finish
    int color; // culoarea pe care o are cand se aplica algoritmul
    struct node* parent;
    struct node* next[]; // nodurile urmatoare in lista de adiacenta
};

struct graph // structura grafului
{
    int vertices; // nr de unghiuri
    int edges; // nr de muchii
    struct node** Adj; //lista de adiacenta
};


// se creeaza un nod cu o cheie data
node* createNode(int x)
{
    node* nod = (node*)malloc(sizeof(node));
    nod->key = x;
    nod->d = 0; // initializare nod
    nod->f = 0;
    nod->color = 0;
    nod->parent = NULL;
    nod->next[0] = NULL;
    return nod;
}


//se creeaza graful cu nr de unghiuri si de muchii date
graph* createGraph(int v, int e)
{
    graph* g = (graph*)malloc(sizeof(graph));
    g->vertices = v;
    g->edges = e;
    g->Adj = (node**)malloc(v*sizeof(node));
    return g;
}

int nr; // nr. de muchii ale grafului
node* addConnection(node* head, node* add) //face legaturile
{
    if (head->key == add->key)
        return head;
    int i = 0;
    while (head->next[i] != NULL)
    {
        if (head->next[i]->key == add->key) // caz in care as avea acelasi varf adiacent, pt a nu-l adauga de 2 ori
            return head;
        i++;
    }
    head->next[i] = add;
    head->next[i + 1] = NULL;
    nr++; // numarul de varfuri adaugate
    return head;
}


int t;
int n;
node** topologically = (node**)malloc(500*sizeof(node));
bool isDAG; //sortarea topologica se poate aplica doar pe grafurile DAG(directet acycle graph)
void DFS_VISIT(graph* g, node* u, int indent) //parametrii: graf, nod
{
    t = t + 1;
    u->d = t; // se seteaza timpul de descoperire
    u->color = 1; // se coloreaza in gri dupa descoperire
    cout << std::setw(indent) << u->key << "\n";
    int j = 0;
    while (u->next[j] != NULL) // se ia fiecare varf adiacent
    {
        if (u->next[j]->color == 0) // daca varful adiacent e alb/nedescoperit
        {
            u->next[j]->parent = u;
            DFS_VISIT(g, u->next[j], indent + 4);
        }

        if (u->next[j]->color == 1) //  cauta daca exista vreun ciclu, fiindca gaseste legatura la un nod care era deja
            //descoperit

        {
            isDAG = false;  //graful nu e DAG
        }
        j++;
    }

    if (isDAG == true) {

        topologically[n] = u;
    }
    n++;
    u->color = 2; // dupa terminare, se coloreaza negru
    t = t + 1;
    u->f = t;
}


void DFS(graph* g)
{
    free(topologically);
    n = 0;
    isDAG = true;
    cout << "\nThe DFS tree resulting from applying the algorithm is:\n";
    for (int i = 0; i < g->vertices; i++)
    {
        g->Adj[i]->color = 0; // fiecare varf se coloreaza alb
        g->Adj[i]->parent = NULL;
    }
    t = 0;
    for (int i = 0; i < g->vertices; i++)
    {
        if (g->Adj[i]->color == 0) // daca e alb
        {
            DFS_VISIT(g, g->Adj[i], 0);
        }
    }
}


int main()
{
// DEMO DFS
    //se creeaza un graf neorientat si conex
    int v[5] = { 0, 1, 2, 3, 4};
    node** nodes = (node**)malloc(10 * sizeof(node)); // tablou de vectori


    //creare/initializare noduri si graf
    for (int i = 0; i < 5; i++)
    {
        nodes[i] = createNode(v[i]);
    }
    graph* g = createGraph(5, 5);

    for (int i = 0; i < g->vertices; i++)
    {
        g->Adj[i] = nodes[i]; //  setez nodurile in ordine, pt a popula lista simplu inlantuita cu nodurile adiacente
    }
    nr = 0;
    addConnection(g->Adj[0], nodes[1]); //se creeaza conexiunile intre varfurile adiacente
    addConnection(g->Adj[0], nodes[2]);
    addConnection(g->Adj[2], nodes[3]);
    addConnection(g->Adj[1], nodes[3]);
    addConnection(g->Adj[3], nodes[4]);



    //afisare graf initial ca si lista de adiacenta
    cout << "The INITIAL graph is:\n";
    for (int i = 0; i < g->vertices; i++)
    {
        cout << g->Adj[i]->key;
        {
            if (g->Adj[i]->next[0] == NULL)
                cout << " has no adjacent edges\n";
            else {
                cout << " is adjacent with: ";
                int j = 0;
                while (g->Adj[i]->next[j] != NULL)
                {
                    cout << g->Adj[i]->next[j]->key << " ";
                    j++;
                }
                cout << "\n";
            }
        }
    }


    //afisare rezult dupa aplicarea DFS
    DFS(g);





    if (isDAG == true) {
        // se afiseaza nodurile din graf in ordine topologica
        cout << "\nThe topological order of the nodes is:\n";
        for (int i = 0; i < n; i++)
        {
            cout << topologically[i]->key << " (" << topologically[i]->d << "/" << topologically[i]->f << ")\n";
        }
    }
    else {
        cout << "Graph is not Directed Acyclic and may not have a Topological ordering of the nodes.";
    }

    _getch();

    return 0;
}

