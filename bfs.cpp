#include "Profiler.h"
#include <stdlib.h>
#include <stdio.h>
#include<limits.h>

typedef enum {WHITE,GRAY,BLACK} COLOR;
#define NR_NODES_TEST 6
#define NR_EDGES_TEST 15
#define NR_NODES 5001
#define NR_EDGES 9001

Profiler profiler("EXTRA");

typedef struct Node {
    int key;
    COLOR color;
    int distance;
    Node* parent;
    Node* next;
}Node;


Node* createNode(int givenKey) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->key = givenKey;
    newNode->next = NULL;
    return newNode;
}

void addEdge(Node* graph[], int u, int v) {
    Node *aux = graph[u]->next;
    graph[u]->next = createNode(v);
    graph[u]->next->next = aux;
}

bool findEdge(Node* graph[], int u, int v) {
    Node* aux = graph[u];
    while (aux != NULL) {
        if (aux->key == v) {
            return true;
        }
        aux = aux->next;
    }
    return false;
}

void free(Node *node) {
    Node*tmp = NULL;
    while (node) {
        tmp = node->next;
        free(node);
        node = tmp;
    }
}



void BFS(Node *graph[], Node *start, int nrNodes,int nrEdges){

    graph[start->key]->color = GRAY;
    graph[start->key]->distance = 0;
    graph[start->key]->parent = NULL;

    int beginQ = 0, endQ = 0;
    int *queue = (int*)malloc(nrNodes * sizeof(int));


    queue[endQ] = start->key;
    ++endQ;
    while (beginQ < endQ) {


        Node *u = graph[queue[beginQ]];
        ++beginQ;
        Node *v = u->next;

        while (v != NULL) {

            if (graph[v->key]->color == WHITE) {
                graph[v->key]->color = GRAY;
                graph[v->key]->distance = u->distance + 1;
                graph[v->key]->parent = u;
                queue[endQ] = v->key;
                ++endQ;
            }
            v = v->next;
        }
        graph[u->key]->color = BLACK;
    }
    free(queue);
}


void testBFS() {

    Node*graph[NR_NODES_TEST];
    for (int i = 0; i < NR_NODES_TEST; ++i) {
        graph[i] = createNode(i);
    }
    addEdge(graph, 0, 1);
    addEdge(graph, 0, 2);
    addEdge(graph, 1, 0);
    addEdge(graph, 1, 2);
    addEdge(graph, 2, 0);
    addEdge(graph, 2, 1);
    addEdge(graph, 4, 2);
    addEdge(graph, 4, 5);
    addEdge(graph, 3, 5);
    addEdge(graph, 5, 3);
    addEdge(graph, 5, 4);

    for (int i = 0; i < NR_NODES_TEST; ++i) {
        graph[i]->color = WHITE;
        graph[i]->distance = INT_MAX;
        graph[i]->parent = NULL;
    }

    printf("\n//////ADJACENCY LIST//////\n\n");
    for (int i = 0; i < NR_NODES_TEST; ++i) {
        printf("%d: ", i);
        Node*aux = graph[i];
        aux = aux->next;
        while (aux != NULL) {
            printf("%d ", aux->key);
            aux = aux->next;
        }
        printf("\n");
    }

    BFS(graph, graph[0], NR_NODES_TEST,NR_EDGES_TEST);
    BFS(graph, graph[3], NR_NODES_TEST,NR_EDGES_TEST);

    printf("\n//////AFTER BFS//////\n\n");
    for (int i = 0; i < NR_NODES_TEST; ++i) {
        if (graph[i]->parent == NULL) {
            printf("%d ROOT\n", i);
        }
        else {
            //printf("node: %d, parent: %d\n", graph[i]->key, graph[i]->parent->key);
        }
        Node*aux = graph[i]->next;
        while (aux != NULL) {
            printf("node: %d, parent: %d\n", graph[i]->key, aux->key);
            aux = aux->next;
        }
    }
}


int main() {


    testBFS();
    getchar();
    return 0;
}