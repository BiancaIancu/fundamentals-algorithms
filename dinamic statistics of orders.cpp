#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>



typedef struct node //structura de tip nod care contine cheia nodului, dimensiunea si adresele catre fiii si parintele nodului
{
    int key;
    int size;
    struct node*left;
    struct node*right;
    struct node*parent;

}node;



node* BUILD_TREE(int i, int j, int x[]){

    if (i <= j) //daca vectorul are elemente
    {
        node *mid = (node*)malloc(sizeof(node));
        mid->key = x[(i + j) / 2];					//radacina primeste valoarea elementului din mijloc
        mid->parent = NULL; //e radacina

        mid->left = BUILD_TREE(i, ((i + j) / 2 - 1), x); //subarborele stang al radacinii se formeaza construind un arbore de la inceputul vectorului pana la jumatate-1
        mid->right = BUILD_TREE(((i + j) / 2 + 1), j, x); //subarborele drept al radacinii se formeaza construind un arbore de la jumatate + 1 pana la sfarsitul vectorului

        mid->size = mid->left->size + mid->right->size+1; // dimensiunea radacininii este suma dimensiunilor celor doi fii
        mid->left->parent = mid; //parintele fiului stang al radacinii este insusi radacina
        mid->right->parent = mid; //parintele fiului drept al radacinii este insusi radacina
        return mid;
    }
    node *mid = (node*)malloc(sizeof(node)); //daca vectorul nu are elemente => nu exista arbore
    mid->key = -1;
    mid->size = 0;
    mid->left = NULL;
    mid->right = NULL;
    mid->parent = NULL;
    return mid;

}

//functie pentru a cauta a i-a cea mai mica valoare
node* OS_SELECT(node *x, int i){

    int r=x->left->size+1;
    if (i==r)
        return x;
    else
        if(i<r)
            return OS_SELECT(x->left, i);
                    else
             return OS_SELECT(x->right, i-r);
}
node *treeMinimum(node *x) //determina nodul cu valoarea minima din arbore (cel mai din stanga nod)
{
    while (x->left->key != -1) // cat timp nodul curent are fiu stang
        x = x->left;

    return x;
}

node *successor(node *x) //determina succesorul unui nod
{
    if (x->right->key != -1) //daca nodul are fiu drept
        return treeMinimum(x->right); //returneaza cel mai din stanga (Cel mai mic) nod din subarborele sau drept, adica succesorul sau

    //daca nu are subarbore drept, atunci succ este parintele subarborelui in care nodul e el maxim
    node *y = x->parent;

    //parcurgem in sus pana gasim
    while (y != NULL && x == y->right)
    {
        x = y;
        y = x->parent;
    }
    return y;
}

void updateSize(node *y)
{
    node* x = y->parent;

    while (x != NULL)				//scadem dimensiunea tuturor nodurilor de pe nivelele anterioare nodului sters (din acelasi subarbore) pana ajungem la radacina
    {
        x->size = x->size -1;
        x = x->parent;
    }
}

//functie pentru stergerea nodului z
node *OS_DELETE(node *root, node *z){
    node *x;
    node *y;
    if(z->left->key==-1 || z->right->key==-1) //nod frunza
        y=z;
    else
        y=successor(z);

    if(y->left->key!=-1)   //nu e frunza
        x=y->left;    //are subarb stang //nu trebuie sa aiba pt ca e min//poate doar sa fie egal
    else
        x=y->right;  //nu are subardb stang, ne folosim de cel drept

    if(y->parent==NULL)  //daca e chiar varful
        root=x;
    else
        if(y==y->parent->left)
            y->parent->left=x;
        else
            y->parent->right=x;  //sterge nodul

    updateSize(y);
    return y;

}

void printInOrder(node *root){
    if (root==NULL)
        return;
        printInOrder(root->left);
        if(root->key!=-1)
        printf("key: %d, size: %d\n", root->key, root->size);
        printInOrder(root->right);


}

int main() {

    srand(time(NULL));
    int n=11;
    int *x = (int *)malloc(n * sizeof(int));
    for(int i=0; i<n; i++){
        x[i]=i+1;
    }

    node *root;
    root=BUILD_TREE(0,n-1,x);
    printInOrder(root);//printeaza nodurile in inordine

    //CAUTARE INDECSI ALEATORI
    int search_key=4;
    node *search=OS_SELECT(root,search_key);
    if(search!=NULL){
        printf("\nAl %d-lea cel mai mic element este\n", search_key );
        printf("\n cheie: %d, size: %d, parent key: %d\n", search->key, search->size, search->parent->key);
    }


    //stergere
    node *deleted=OS_DELETE(root,search);
    search->key=deleted->key;
    free(deleted);
    printf("\n Arborele dupa stergerea elementului\n");
    printInOrder(root);

    //cautare
    int search_key2=7;
    node *search2=OS_SELECT(root,search_key);
    if(search2!=NULL){
        printf("\nAl %d-lea cel mai mic element este\n ",search_key2 );
        printf("\n cheie: %d, size: %d, parent key: %d\n", search2->key, search2->size, search2->parent->key);
    }

    //cautare
    int search_key3=9;
    node *search3=OS_SELECT(root,search_key);
    if(search3!=NULL){
        printf("\nAl %d-lea cel mai mic element este\n" ,search_key3 );
        printf("\n cheie: %d, size: %d, parent key: %d\n", search3->key, search3->size, search3->parent->key);
    }
    return 0;
}