
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int T = 0;

typedef struct node
{
    int key;
    struct node *next;
}Node;

Node *heap[500];
int heapSize;

Node *result;
Node *last;

//SORTAREA ELEMENTELOR IMPARTITE
Node* sortedMerge(Node *a, Node *b)
{
    Node *final = NULL;

    if (a == NULL)
    {
        return b;
    }
    else if (b == NULL)
    {
        return a;
    }

    if (a->key < b->key)
    {
        final = a;
        final->next = sortedMerge(a->next, b);
    }
    else
    {
        final = b;
        final->next = sortedMerge(a, b->next);
    }

    return final;
}

void Split(Node *source, Node **front, Node **back)
{
    Node *fast;
    Node *slow;

    slow = source;
    fast = source->next; //2 noduri la distanta de 1

    while (fast != NULL)
    {
        fast = fast->next;

        if (fast != NULL)
        {
            slow = slow->next;
            fast = fast->next;
        }
    } //slow va ajunge la jumatate

    *front = source; //primul element
    *back = slow->next; //elementul din mijloc
    slow->next = NULL;
}

void mergeSort(Node **head)
{
    Node *first = *head;
    Node *a;
    Node *b;

    if ((first == NULL) || (first->next == NULL))
    {
        return; //LISTA E GOALA SAU EXISTA DOAR UN ELEMENTE
        //DECI NU AM CE SORTA
    }

    Split(first, &a, &b);
    //RECURSIV => impartirea algoritmului MergeSort
    //a va fi primul element
    mergeSort(&a);
    //b va fi elementul din mijloc
    mergeSort(&b);


    *head = sortedMerge(a, b);

}


//CREAZA O LISTA
Node *creareLista(int size, int i)
{
    Node *first = (Node *)malloc(sizeof(Node));
    first->key = rand() % 100;
    first->next = NULL;

    Node *prev = first;


    for (int j = 1; j < size; j++)
    {
        Node *nextNode = (Node*)malloc(sizeof(Node));
        nextNode->key = rand() % 100;
        nextNode->next = NULL;

        prev->next = nextNode;
        prev = prev->next;
    }

    mergeSort(&first);

    return first;
}

//CREAZA MAI MULTE LISTE
void creareListe(int k, int n)
{
    //int *lengthVector = (int *)malloc(k*sizeof(int));
    //createLengths(n, k, lengthVector);

    for (int i = 0; i < k; i++)
    {
        heap[i] = (Node *)malloc(sizeof(Node));
        heap[i] = creareLista(n/k,i);
        //n/k este numarul de elemente ale unei liste si
        // i este numarul listei
    }
}

int parent(int i)
{
    return (i - 1) / 2;
}

int leftChild(int i)
{
    return 2 * i + 1;
}

int rightChild(int i)
{
    return 2 * i + 2;
}

void swap(int i,int j)
{
    Node *aux = heap[i];
    heap[i] = heap[j];
    heap[j] = aux;
}

void heapify(int i)
{
    int minIndex = i;
    int left = leftChild(i);
    int right = rightChild(i);

    T++;
    if ((left < heapSize) && heap[minIndex]->key > heap[left]->key)
    {
        minIndex = left;
    }

    T++;
    if ((right<heapSize) && heap[minIndex]->key > heap[right]->key)
    {
        minIndex = right;
    }

    if (minIndex != i)
    {
        T = T + 3;

        swap(i,minIndex);
        heapify(minIndex);
    }
}

void buildHeap(int k)
{
    int i;
    for (i = parent(heapSize - 1); i >= 0; i--)
    {
        heapify(i);
    }
}

void enqueue(int key)
{
    Node *newNode = (Node *)malloc(sizeof(Node));
    T=T+2;
    newNode->next = NULL;
    newNode->key = key;

    T = T + 2;
    if (last==NULL)
    {
        result = newNode;
    }
    else
    {
        last->next = newNode;
    }

    T++;
    last = newNode;
}

void printResult()
{
    Node *t = result;

    while (t)
    {
        printf("%d ", t->key);
        t = t->next;
    }
}

Node *mergeKLists(int k, int n)
{
    int count = 0;
    heapSize = k;

    buildHeap(k);


    while (count<n)
    {


        enqueue(heap[0]->key);
        count++;

        T++;
        if (heap[0]->next == NULL)
        {

            for (int index = 0; index < heapSize - 1; index++)
            {
                T++;
                heap[index] = heap[index + 1];
            }
            heapSize = heapSize - 1;

        }
        else
        {
            T = T + 2;
            Node *deleted = heap[0];
            heap[0] = heap[0]->next;
            free(deleted);
        }

        heapify(0);
    }

    T++;
    while (heap[0] != NULL)
    {
        T = T + 3;

        enqueue(heap[0]->key);
        Node *deleted = heap[0];
        heap[0] = heap[0]->next;

        free(deleted);
        count++;
    }

    return result;
}

void printList(int i)
{
    Node *t = heap[i];

    while (t)
    {
        printf("%d ", t->key);
        t = t->next;
    }

    printf("\n");
}


int main() {
    //TEST: exemplu n=20, k=4

    srand(time(NULL));
    creareListe(4,20);
    printf("Listele generate si sortate\n");

    printList(0);
    printf("\n");
    printList(1);
    printf("\n");
    printList(2);
    printf("\n");
    printList(3);
    printf("\n");

    Node *result=mergeKLists(4, 20);
    printf("\n");

    printf("Rezultatul este este:\n");
    printResult();

//CAZUL MEDIU STATISTIC
    //ex1
    FILE *f1 = fopen("Grafic1.csv", "w");
    FILE *f2 = fopen("Grafic2.csv", "w");

    if (f1 == NULL)
    {
        printf("Error file f1");
        exit(-1);
    }

    if (f2 == NULL)
    {
        printf("Error file f1");
        exit(-1);
    }

    fprintf(f1, "n,k1,T1,k2,T2,k3,T3\n");
    fprintf(f2, "k,n,T\n");

    int n;
    int k;
    int k1 = 5;
    int k2 = 10;
    int k3 = 100;
    for (n = 100; n <= 10000; n = n + 100)
    {
        creareListe(k1, n);
        mergeKLists(k1, n);

        fprintf(f1, "%d,%d,%d,", n, k1, T);

        T = 0;

        creareListe(k2, n);
        mergeKLists(k2, n);


        fprintf(f1, "%d,%d,", k2, T);

        T = 0;

        creareListe(k3, n);
        mergeKLists(k3, n);

        fprintf(f1, "%d,%d\n", k3, T);

        T = 0;
    }

    //ex2
    for (k = 10; k <= 500; k = k + 10)
    {
        creareListe(k, 10000);
        mergeKLists(k, 10000);

        fprintf(f2, "%d,%d,%d\n", k, 4000, T);

        T = 0;
    }

    fclose(f1);
    fclose(f2);
    return 0;
}