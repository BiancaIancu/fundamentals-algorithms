#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#define N 9973
using namespace std;

int A[N];
int nr,suma,NFsuma;
int inter[1500];
int factorUmplere[5];

int h(int cheie,int i)
{   //numere prime intre ele
    return(cheie + 14*i + 19*(i*i)) % N;
}
void initializare()
{
    for(int i = 0 ; i < N ; i++)
    {
        A[i] = -1;
    }
}
int insereaza(int A[], int cheie)
{
    int i = 0;
    do
    {
        int j = h(cheie,i);
        if(A[j] == -1)
        {
            A[j] = cheie;
            return j;
        }
        else
        {
            i = i + 1;
        }
    }
    while(i < N);

    if(i > N)
    {
        return 0;
    }
}

int searchH(int A[], int cheie)
{
    int i = 0;
    int j;
    do
    {
        j = h(cheie, i);
        nr ++;
        if(A[j] == cheie)
        {
            return j;
        }
        else
        {
            i = i + 1;
        }
    }
    while(A[j] == -1 && i < N);
    return -1;
}
int main()
{

    FILE *f1 = fopen("Hash.csv", "w");


    factorUmplere[0] = 7978;
    factorUmplere[1] = 8475;
    factorUmplere[2] = 8976;
    factorUmplere[3] = 9475;
    factorUmplere[4] = 9874;
    fprintf(f1, "Factorul de umplere, Efort mediu gasite, Efort maxim gasite, Efort mediu negasite, Efort maxim negasite\n");
    for(int z = 0 ; z < 5 ; z++) {
        initializare();
        float aux = 0;
        int k = 0;

        int t = 0;
        int efortMaxim = 0;
        int m = 0;
        nr = 0;
        suma = 0;
        NFsuma = 0;
        float efortMediu = 0;
        float NGefortMediu = 0;
        int NGefortMaxim = 0;

        for (int i = 0; i < factorUmplere[z]; i++) {
            k = rand() % 10000 + 1;
            t = insereaza(A, k);
            if (t != 0 && t != -1) {
                m = m + 1;
            }
        }
        aux = (float) m / N;
        printf("\nFactorul de umplere: %.2f ", aux);
        fprintf(f1,"\n%.2f,", aux);


        for (int i = 0; i < 1500; i++) {
            inter[i] = A[rand() % factorUmplere[z]];
        }
        efortMaxim = 0;
        for (int i = 0; i < 1500; i++) {
            nr = 0;
            k = inter[i];
            t = searchH(inter, k);
            if (nr > efortMaxim) {
                efortMaxim = nr;
            }
            suma += nr;
        }

        efortMediu = (float) suma / 1500;
        printf("G Efort mediu: %d", efortMediu);
        printf("G Efort maxim: %d", efortMaxim);
        fprintf(f1,"%d, %d,", efortMediu,efortMaxim);

        NGefortMediu = 0;
        NGefortMaxim = 0;

        for (int i = 0; i < 1500; i++) {
            nr = 0;
            k = rand() % 10000 + 20000;
            t = searchH(A, k);

            if (nr > NGefortMaxim) {
                NGefortMaxim = nr;
            }
            NFsuma += nr;
        }

        NGefortMediu = (float) NFsuma / 1500;
        printf("NG Efort mediu: %d ", NGefortMediu);
        printf("NG Efort maxim: %d ", NGefortMaxim);
        fprintf(f1, "%d, %d", NGefortMediu, NGefortMaxim);


        //fprintf(f1, "%d, %d, %d, %d, %d", aux, efortMediu, efortMaxim, NGefortMediu, NGefortMaxim);
    }
    return 0;
}
