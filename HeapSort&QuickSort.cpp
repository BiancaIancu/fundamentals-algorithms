#include <conio.h>
#include <stdio.h>
#include <cstdlib>
#include <math.h>
#include <time.h>
#define _CRT_SECURE_NO_WARNINGS

int sumaHeapSort;
int sumaQuickSort;

int partition(int v[], int a, int b){
    int v2=v[b];
    int i=a-1;
    int aux;

    for(int j=a; j<b-1; j++){
        sumaQuickSort=sumaQuickSort+1;
        if(v[j]<=v2){
            sumaQuickSort=sumaQuickSort+4;
            i++;
            aux=v[i];
            v[i]=v[j];
            v[j]=aux;
        }
    }
    sumaQuickSort=sumaQuickSort+3;
    aux=v[i+1];
    v[i+1]=v[b];
    v[b]=aux;

    return i+1;

}

void heapify(int a[], int i, int n) {
//INDECSI
    int l, r;
    int aux;
    //fie maximul ca radacina
    int max = i;
    //stanga
    l = 2 * i;
    //dreapta
    r = 2 * i + 1;

    sumaHeapSort+=2;

    sumaHeapSort+=2;
    if (l <= n && a[l] > a[max])
    {
        max = l;
        sumaHeapSort++;
    }

    sumaHeapSort+=2;
    if (r <= n && a[r] > a[max])
    {
        max = r;
        sumaHeapSort++;
    }

    sumaHeapSort++;
    if (max != i)
    {

        aux = a[i];
        a[i] = a[max];
        a[max] = aux;
        heapify(a, max, n);
        sumaHeapSort += 3;
    }
}
void createMaxHeap(int v[], int n){
    int i;
    for(i=n/2; i>=1; i--)
        heapify(v,i,n);
}
void heapSort(int a[], int n){

    int aux;
    int heap_size=n;
    createMaxHeap(a,n);
    for(int i=n; i>=2; i--){
        aux=a[1];
        a[1]=a[i];
        a[i]=aux;
        heap_size=heap_size-1;
        sumaHeapSort=sumaHeapSort+4;
        heapify(a,1,heap_size);

    }
}
void quickSort(int v[], int a , int b){
    int x;
    sumaQuickSort=sumaQuickSort+1;
    if(a<b){
        x=partition(v,a,b);
        sumaQuickSort=sumaQuickSort+1;
        quickSort(v,a,b-1);
        quickSort(v,a+1,b);
    }

}

void mediumStatistic(int v1[], int v2[], int n){
    for(int i=0; i<n; i++) {
        v1[i] = rand();
        v2[i]=v1[i];
    }
}
int main() {

    FILE *f=fopen("HeapSort&QuickSort.csv","w");
    fprintf(f,"n,HeapSort,QuickSort\n");
    srand(time(NULL));
    int *v1=NULL;
    int *v2=NULL;

    for(int i=100; i<=10000; i=i+100){
        v1=(int*)malloc(i*sizeof(int));
        v2=(int*)malloc(i*sizeof(int));

        //CELE 5 MASURATORI
        for(int j=0; j<5; j++) {
            mediumStatistic(v1,v2,i); //GENERAM CELE 2 SIRURI CU VALORI RANDOM, EGALE FIIND (si cu lungimea i)
            heapSort(v2,i);

            for(int k=1; k<i; k++)
                v2[k]=v1[k]; //se reface vectorul 2

            quickSort(v2,1,i);
        }

        sumaHeapSort=sumaHeapSort/5;
        sumaQuickSort=sumaQuickSort/5;

        fprintf(f, "%d,%d,%d\n",i,sumaHeapSort,sumaQuickSort);

    }
    fclose(f);
    return 0;
}