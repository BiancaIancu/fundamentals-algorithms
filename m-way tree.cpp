
#include <stdio.h>
#include <stdint.h>
#include <conio.h>
#include <stdlib.h>
#define MAX_CHILDREN 3
#define NR_NODES 9
int a[] = { 6, 2, 7, 5, 2, 7, 7, -1, 5,2 };

typedef struct nodeR2
{
    int key;
    int noChildrens;
    struct nodeR2*childrenArray[MAX_CHILDREN];

}nodeR2;

struct nodeR2*R2Array[NR_NODES];
struct nodeR2*rootR2;


typedef struct nodeR3
{
    int key;
    struct nodeR3*down;
    struct nodeR3*right;

}nodeR3;


void parentRepresentation (){
    for(int i=0; i<NR_NODES; i++){
        if (a[i]==-1){
            printf(" %d ", i);
        }
        else{
            printf("%d ", i);
        }
    }

    printf("\n");

    for(int i=0; i<NR_NODES; i++){
        printf("%d ", a[i]);
    }

}

void CreateMultiway(){
    for (int i = 0; i < NR_NODES; i++){

        R2Array[i] = (nodeR2*)malloc(sizeof(nodeR2));
        R2Array[i]->key = i;
        R2Array[i]->noChildrens = 0;
    }

    for (int i = 0; i < NR_NODES; i++){
        int parent = a[i];
        int child =i;
        if (a[i] != -1){
            R2Array[parent]->childrenArray[R2Array[parent]->noChildrens] = R2Array[child];
            R2Array[parent]->noChildrens++;
        }
        else{
            rootR2 = R2Array[child];
        }
    }
}

void MultiWayPrint(nodeR2*root){


    if (root->noChildrens > 0){
        printf("Parent %d: ", root->key);
        for (int i = 0; i < root->noChildrens; i++){
            printf("%d ", root->childrenArray[i]->key);
        }
        printf("\n");
        for (int i = 0; i < root->noChildrens; i++){
            MultiWayPrint(root->childrenArray[i]);
        }
    }

}

nodeR3* newNode(int key){

    nodeR3* result = (nodeR3*)malloc(sizeof(nodeR3));
    result->key = key;
    result->down = NULL;
    result->right = NULL;
    return result;
}


void CreateBinaryTree(nodeR3*rootR3, nodeR2*rootR2){

    if (rootR2 != NULL){

        if (rootR2->noChildrens > 0){

            rootR3->down = newNode(rootR2->childrenArray[0]->key);
            CreateBinaryTree(rootR3->down,rootR2->childrenArray[0]);
            rootR3 = rootR3->down;
            for (int i = 1; i < rootR2->noChildrens;i++){
                rootR3->right = newNode(rootR2->childrenArray[i]->key);
                CreateBinaryTree(rootR3->right, rootR2->childrenArray[i]);
                rootR3 = rootR3->right;
            }
        }
    }
}


void prettyPrintBinary(nodeR3*root, int depth){

    if (root != NULL){
        for (int i = 0; i < depth; i++){
            printf("   ");
        }
        printf("%d \n", root->key);
        depth++;
        prettyPrintBinary(root->down, depth);
        depth--;
        prettyPrintBinary(root->right, depth);
    }
    else{
        return;
    }
}


int main()
{

    printf("Parent representation:\n");
    parentRepresentation();


    CreateMultiway();
    printf("\n\nMULTI-WAY TREE REPRESENTATION: \n");
    printf("Root: %d\n", rootR2->key);
    MultiWayPrint(rootR2);

    struct nodeR3*rootR3;
    rootR3 = newNode(rootR2->key);
    CreateBinaryTree(rootR3, rootR2);

    printf("\n\nBINARY TREE REPRESENTATION: \n");
    prettyPrintBinary(rootR3, 0);


    return 0;
}
















